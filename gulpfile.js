/**
 * Configuracion.
 */

// Proyecto.
var project                 = 'TacticChild'; // Nombre del proyecto.
var projectURL              = 'http://localhost/'; // Directorio local de trabajo.
var productURL              = './'; // URL del theme o plugin. No es necesario modificar pues se utiliza la carpeta raíz siempre.

// Estilos.
var styleSRC                = './src/sass/tactic_center_child.scss'; // Ruta al archivo SASS principal.
var styleDestination        = './css/'; // Ruta del archivo final compilado.

// JavaScripts.
var jsCustomSRC             = './src/js/*.js'; // Ruta a la carpeta con los archivos javascripts.
var jsCustomDestination     = './js/'; // Ruta del archivo final compilado.
var jsCustomFile            = 'tactic_center_child'; // Nombre del archivo final.

// Rutas de archivos a chequear cuando se realicen cambios.
var styleWatchFiles         = './src/sass/*.scss'; // Ruta a todos los archivos Sass.
var customJSWatchFiles      = './src/js/*.js'; // Ruta a todos los archivos JS.
var projectPHPWatchFiles    = './**/*.php'; // Ruta a todos los archivos PHP.


// Agrega autoprefies para distintos navegadores.
// Browserlist https://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
  ];

/**************** FIN EDICION DE VARIABLES *********************/

var gulp         = require('gulp');
var sass         = require('gulp-sass');
var minifycss    = require('gulp-uglifycss');
var autoprefixer = require('gulp-autoprefixer');
var mmq          = require('gulp-merge-media-queries');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var rename       = require('gulp-rename');
var lineec       = require('gulp-line-ending-corrector');
var filter       = require('gulp-filter');
var sourcemaps   = require('gulp-sourcemaps');
var notify       = require('gulp-notify');
var browserSync  = require('browser-sync').create();
var zip 		 = require('gulp-zip');
var reload       = browserSync.reload;

/**
 * Tarea 'browser-sync'.
 * Refresca el navegador con los cambios realizados.
 */
gulp.task( 'browser-sync', function() {
  browserSync.init( {

    proxy: projectURL,
    open: true,
    injectChanges: true,

    // Usa un puerto específico.
    // port: 7000,

  } );
});

/**
 * Tarea: 'styles'.
 * Compila Sass.
 */
 gulp.task('styles', function () {
    gulp.src( styleSRC )
    .pipe( sourcemaps.init() )
    .pipe( sass( {
      errLogToConsole: true,
      outputStyle: 'compact',
      precision: 10
    } ) )
    .on('error', console.error.bind(console))
    .pipe( sourcemaps.write( { includeContent: false } ) )
    .pipe( sourcemaps.init( { loadMaps: true } ) )
    .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

    .pipe( sourcemaps.write ( './' ) )
    .pipe( lineec() )
    .pipe( gulp.dest( styleDestination ) )

    .pipe( filter( '**/*.css' ) )
    .pipe( mmq( { log: true } ) )

    .pipe( browserSync.stream() )
 });

 /**
  * Tarea: 'stylesmin'.
  * Compila Sass y produce un archivo CSS minificado.
  */
  gulp.task('stylesmin', function () {
     gulp.src( styleSRC )
     .pipe( sourcemaps.init() )
     .pipe( sass( {
       errLogToConsole: true,
       // outputStyle: 'compact',
       outputStyle: 'compressed',
       // outputStyle: 'nested',
       // outputStyle: 'expanded',
       precision: 10
     } ) )
     .on('error', console.error.bind(console))
     .pipe( sourcemaps.write( { includeContent: false } ) )
     .pipe( sourcemaps.init( { loadMaps: true } ) )
     .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

     .pipe( sourcemaps.write ( './' ) )
     .pipe( lineec() )
     .pipe( gulp.dest( styleDestination ) )

     .pipe( filter( '**/*.css' ) )
     .pipe( mmq( { log: true } ) )

     .pipe( browserSync.stream() )

     .pipe( rename( { suffix: '.min' } ) )
     .pipe( minifycss( {
       maxLineLen: 0
     }))
     .pipe( lineec() )
     .pipe( gulp.dest( styleDestination ) )

     .pipe( filter( '**/*.css' ) )
     .pipe( browserSync.stream() )
     .pipe( notify( { message: 'TASK: "stylesmin" Completed! 💯', onLast: true } ) )
  });


  /**
  * Task: `scripts`.
  * Compila los JS y crea un archivo minificado.
  */
 gulp.task( 'scripts', function() {
    gulp.src( jsCustomSRC )
    .pipe( concat( jsCustomFile + '.js' ) )
    .pipe( lineec() )
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( rename( {
      basename: jsCustomFile,
      suffix: '.min'
    }))
    .pipe( uglify() )
    .pipe( lineec() )
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( notify( { message: 'TASK: "customJs" Completed! 💯', onLast: true } ) );
 });


 /**
  * Comando Gulp por defecto.
  * Detecta los cambios en los archivos y refresca el navegador.
  */
 gulp.task( 'default', ['styles', 'stylesmin', 'scripts', 'browser-sync'], function () {
  gulp.watch( projectPHPWatchFiles, reload );
  gulp.watch( styleWatchFiles, [ 'styles' ] );
  gulp.watch( customJSWatchFiles, [ 'scripts', reload ] );
 });

// Empaqueta el theme para distribución.
gulp.task('zip', function () {
   gulp.src([
    './**/*',
    '!./{node_modules,node_modules/**/*}',
    '!./{src,src/**/*}',
    '!./gulpfile.js',
    '!./package.json',
	 '!./package-lock.json'
  ])
    .pipe( zip('child_theme.zip') )
    .pipe( gulp.dest('./../') );
});
