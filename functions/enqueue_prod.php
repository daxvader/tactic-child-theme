<?php
/**
 * Enqueue scripts para producción.
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'wp_enqueue_scripts', 'tactic_center_child', 10 );
add_action( 'login_enqueue_scripts', 'tactic_center_child', 10); // Inserta los estilos en la pantalla de login.
add_action( 'wp_enqueue_scripts', 'tactic_center_scripts', 999 );

// Inserta los archivos CSS del tema.
if ( !function_exists( 'tactic_center_child' ) ) :
  function tactic_center_child() {
    wp_enqueue_style( PARENT_STYLE, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'tactic_center_child', get_stylesheet_directory_uri() . '/css/tactic_center_child.min.css', array(), '1.0.0' );
  }
endif;

/*
 * Inserta los archivos JS.
 * Si se desea que el script se cargue en el header, el último parámetro debe ser 'false'.
 * Por defecto debe ser 'true' para cargarse en el footer.
 *
 */
if ( ! function_exists( 'tactic_center_scripts' ) ) :
	function tactic_center_scripts() {
		wp_enqueue_script( 'tactic_center_scripts', get_stylesheet_directory_uri() . '/js/tactic_center_child.min.js', array(), '1.0.0', true );
	}
endif;
