<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Nombre de la plantilla padre. Puede ser 'divi', por ejemplo.
const PARENT_STYLE = 'twentyseventeen';

// Estilos y scripts para desarrollo.
require_once( get_stylesheet_directory().'/functions/enqueue_dev.php' );

// Estilos y scripts para producción.
// require_once( get_stylesheet_directory().'/functions/enqueue_prod.php' );
